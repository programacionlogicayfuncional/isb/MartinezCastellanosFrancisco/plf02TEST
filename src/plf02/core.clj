(ns plf02.core)

;PREDICADOS
;associative?
(defn función-associative?-1
  [a b c ]
   (associative? [a b c]))

(defn función-associative?-2
  [c d e]
   (associative? #{c d e}))

(defn función-associative?-3
  [ f g]
  (associative? {f g}))

(función-associative?-1 2 3 4)
(función-associative?-2 4 5 6 )
(función-associative?-3 1 3)

;boolean?

(defn función-boolean?-1
  [a]
  (boolean? a))

(defn función-boolean?-2
  [b]
  (boolean? (new Boolean b)))

(defn función-boolean?-3
  [c]
  (boolean? c))


(función-boolean?-1 0)
(función-boolean?-2 false)
( función-boolean?-3 nil)


